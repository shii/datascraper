<?php
/**
 * Short description:
 * System bootstrap file
 *
 */

/**
 * Open error reporting.
 */

error_reporting(E_ALL);

$basedir = realpath(__DIR__);

/**
 *
 * Checks namespaces and loads classes automatically.
 * @param $class class names with namespace
 */
spl_autoload_register(function ($class) { //echo $class;
    $path = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($path))

        require_once __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';

    require_once __DIR__ . '/vendor/autoload.php';
});
