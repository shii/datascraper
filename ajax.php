<?php

require_once __DIR__ . '/bootstrap.php';

$get = $_GET;

use classes\DataScrapper;

$obj = new DataScrapper();


$data = $obj->connect($get['searchterm']);
//var_dump($data);
$obj->scrap($data);

$sort = $obj->parse();

$result = $obj -> sortData($sort);

echo json_encode($result);