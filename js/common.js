$(function(){

    $('#search-items').click(function(e){
            e.preventDefault();
            var $this = $(this);
            errClass();
            var $searchterm = '';
                $searchterm = $('#query').val();
                if($searchterm !== ""){
                    $this.prop('disabled', true);
                    $this.text('Wait ...');

                    $.get( "ajax.php",{searchterm : $searchterm}, function( data ) {

                        console.log(data);
                        $( "#result" ).load( "result.php", { "param": data },function() {
                            $this.prop('disabled', false);
                            $this.text('Search');

                        });



                    });

                } else {
                    errClass(true);

                }

    });

});

function errClass(err=false){
    if(err){
        $('.form-group').addClass('has-error');
        $('.help-block').html('Search term is required');
    } else{
        $('.form-group').removeClass('has-error');
        $('.help-block').html('');
    }

}