<?php
$param = $_POST['param'];
$result = json_decode($param);
//print_r($result);
?>
<table class="table table-striped">
    <?php if(!empty($result)) {?>
    <thead><tr>
            <th width="50%">Title</th>
            <th>Image</th>
            <th>Price</th>
    </tr>
    </thead>
    <tbody>
            <?php foreach($result as $rst){ ?>
                <tr><td><?php echo $rst->title ?></td>
                <td><img src="<?php echo $rst->src; ?>" class="" alt="<?php echo $rst->title; ?>" title="<?php echo $rst->title; ?>"></td>
                <td><?php echo $rst->currency.$rst->price; ?></td></tr>
            <?php  } ?>

    </tbody>
    <?php } else {?>
        <tr class="warning"><td>No Results found</td></tr>
    <?php } ?>
</table>

