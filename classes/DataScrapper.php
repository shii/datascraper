<?php


namespace classes;

use Sunra\PhpSimple\HtmlDomParser;

class DataScrapper {

    public $url = 'http://www.amazon.com/s/ref=nb_sb_ss_i_2_3?url=field-keywords=';
    public $items = [];

    /*
     * Connecting to outward URLs, using curl
     * @param $term string
     * @return html data
     */
    function connect( $term =''){
       $term = $this -> strReplace($term);
       $this->url = $this->url.$term;

        $ch = curl_init();  // Initialising cURL
        curl_setopt($ch, CURLOPT_URL, $this->url);    // Setting cURL's URL option with the $url variable passed into the function
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // Setting cURL's option to return the webpage data
        $data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
        curl_close($ch);    // Closing cURL

        return $data; // Returning the data from the function

    }

    /*
     * Get the desired content from the raw html
     * @param $data string html
     *
     */

    function scrap($data){
        $dom = HtmlDomParser::str_get_html( $data);

        foreach($dom->find('li.s-result-item div.s-item-container div.a-fixed-left-grid div.a-fixed-left-grid-inner') as $li) {
            $item['src']     = $li->find('img[src]' , 0)->attr['src'] ;
            $item['title']   = $li->find('h2' ,0)->plaintext;

            $item['price_detail'] = $li->find('a span',0)->plaintext;
            // $item['details'] = $li->find('div.price-box span span');
            $this -> items[] = $item;

        }

    }

    /*
     * Convert the items in sortable array
     * sort key is price
     */
    function parse(){
        $sort = [];
        foreach($this->items as  &$item){
            $item['currency'] = $item['price_detail'][0];
            $item['price'] = substr($item['price_detail'] , 1);
            $sort[]= $item['price'];
        }
        return $sort;

    }

    /*
     * sort items array by price key
     * @return items as final result
     */

    function sortData($sort){

           array_multisort($sort , SORT_DESC , $this->items);
           return $this->items;

    }

    private function strReplace($string){
        return str_replace(' ', '+', $string);
    }

} 